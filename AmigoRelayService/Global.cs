﻿using Dlp.Buy4.Log;
using Dlp.Buy4.Log.Listeners;

namespace AmigoRelayService
{
    public static class Global
    {
        private static LogWriter logWriter = new LogWriter(new EnterpriseLibraryLogListener());

        /// <summary>
        /// Logs message with error category.
        /// </summary>
        /// <param name="Message">The message to be logged</param>
        public static void LogError(string Message)
        {
            logWriter.Write(Message, LogCategory.Error);
        }

        /// <summary>
        /// Logs message with Warning category.
        /// </summary>
        /// <param name="Message">The message to be logged</param>
        public static void LogWarning(string Message)
        {
            logWriter.Write(Message, LogCategory.Warning);
        }

        /// <summary>
        /// Logs message with Information category.
        /// </summary>
        /// <param name="Message">The message to be logged</param>
        public static void LogInfo(string Message)
        {
            logWriter.Write(Message, LogCategory.Information);
        }

        /// <summary>
        /// Logs message with Undefined category.
        /// </summary>
        /// <param name="Message">The message to be logged</param>
        public static void LogUndefined(string Message)
        {
            logWriter.Write(Message, LogCategory.Undefined);
        }
    }
}