﻿using System;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;

namespace AmigoRelayService
{
    public class AmigoRelayService : IAmigoRelayService
    {
        public int CreateTicket(Ticket ticket)
        {
            Global.LogInfo("Creating ticket with subject : " + ticket.Subject);
            return FreshService.FreshServiceInterface.CreateTicket(ticket);
        }
    }
}
