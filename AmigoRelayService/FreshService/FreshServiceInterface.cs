﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;
using AmigoRelayService.FreshService.Extensions;

namespace AmigoRelayService.FreshService
{
    public static class FreshServiceInterface
    {
        public static int CreateTicket (Ticket t)
        {
            string freshDeskDomain = ConfigurationManager.AppSettings["AmigoDomain"];
            string freshDeskAPIKey = ConfigurationManager.AppSettings["AmigoApiKey"];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(freshDeskDomain);
            request.ContentType = "application/json";
            request.Method = "POST";


            string json = new JavaScriptSerializer().Serialize(t.ToFreshServiceDTO());
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentLength = byteArray.Length;


            string authInfo = freshDeskAPIKey + ":X"; // It could be your username:password also.
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
            Stream dataStream = request.GetRequestStream();            
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            try
            {
                Global.LogInfo("Calling FreshService API");
                WebResponse response = request.GetResponse();
                Stream a = response.GetResponseStream();
                byte[] responseArray = new byte[response.ContentLength];
                a.Read(responseArray, 0, (int)response.ContentLength);
                string responseString = System.Text.Encoding.Default.GetString(responseArray);
                return 200;
            }
            catch (Exception ex)
            {
                Global.LogInfo("There was an error calling FreshService API : " + ex.ToString());
                throw ex;
            }
        }
    }
}