﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmigoRelayService.FreshService.Extensions
{
    public static class TicketRequestMapper
    {
        public static TicketRequest ToFreshServiceDTO(this Ticket t)
        {
            TicketRequest myTicketRequest = new TicketRequest();
            myTicketRequest.cc_emails = "";

            HelpDeskTicket myHelpDeskTicket = new HelpDeskTicket();
            myTicketRequest.helpdesk_ticket = myHelpDeskTicket;


            myHelpDeskTicket.subject = t.Subject;
            myHelpDeskTicket.status = (int)t.Status;
            myHelpDeskTicket.email = t.Email;
            myHelpDeskTicket.description = t.Description;
            myHelpDeskTicket.description_html = "";
            myHelpDeskTicket.priority = (int)t.Priority;

            return myTicketRequest;
        }
    }
}