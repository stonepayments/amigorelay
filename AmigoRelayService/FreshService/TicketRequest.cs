﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmigoRelayService.FreshService
{
    public class HelpDeskTicket
    {
        //public int display_id { get; set; }

        public string email { get; set; }

        //public int requester_id { get; set; }

        /*public int assoc_problem_id { get; set; }

        public int assoc_change_id { get; set; }

        public int assoc_change_cause_id { get; set; }

        public int assoc_asset_id { get; set; }*/

        public string subject { get; set; }

        public string description { get; set; }

        public string description_html { get; set; }

        public int status { get; set; }

        public int priority { get; set; }

        public int source { get; set; }

        //public bool deleted { get; set; }

        //public bool spam { get; set; }

        /*public int responder_id { get; set; }

        public int group_id { get; set; }

        public int ticket_type { get; set; }

        public List<string> to_email { get; set; }

        public List<string> cc_email { get; set; }

        public int email_config_id { get; set; }*/

        //public bool isescalated { get; set; }

        //public DateTime due_by { get; set; }

        //public int id { get; set; }

        //public List<object> attachments { get; set; }
    }

    public class TicketRequest
    {
        public HelpDeskTicket helpdesk_ticket { get; set; }

        public string cc_emails { get; set; }
    }
}