﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace AmigoRelayService
{
    /// <summary>
    /// General ticket object, used to be relayed
    /// </summary>

    [DataContract]
    public class Ticket
    {
        /// <summary>
        /// The requester email, used to send ticket updates, mandatory
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// The subject of the ticket
        /// </summary>
        [DataMember]
        public string Subject { get; set; }

        /// <summary>
        /// The description of the ticket
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// The status of the ticket
        /// </summary>
        [DataMember]
        public StatusEnum Status { get; set; }

        /// <summary>
        /// The priority of the ticket
        /// </summary>
        [DataMember]
        public PrioritiyEnum Priority { get; set; }

        /// <summary>
        /// Enum of possible source types
        /// </summary>
        public enum SourceTypeEnum
        {
            Email = 1,
            Portal = 2,
            Phone = 3,
            Chat = 4
        }

        /// <summary>
        /// Enum of possible ticket status
        /// </summary>
        public enum StatusEnum
        {
            Open = 2,
            Pending = 3,
            Resolved = 4,
            Closed = 5
        }

        /// <summary>
        /// Enum of possible priorities to be assigned to a ticket
        /// </summary>
        public enum PrioritiyEnum
        {
            Low = 1,
            Medium = 2,
            High = 3,
            Urgent = 4
        }
    }
}